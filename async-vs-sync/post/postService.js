// postService.js

const axios = require('axios');

class PostService {
  constructor() {
    this.posts = [];
    this.nextPostId = 1;
  }

  createPost(title, content) {
    const post = { id: this.nextPostId, title, content };
    this.posts.push(post);
    this.nextPostId++;
    this.sendEvent('postCreated', post);
    console.log('event created: postCreated')

    return post;
  }

  sendEvent(event, data) {
    axios.post('http://localhost:5000/events/post', { event, data });
  }

  getPosts() {
    return this.posts;
  }
}

module.exports = new PostService();
