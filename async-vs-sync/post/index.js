// app.js (Post Service)

const express = require('express');
const postService = require('./postService');

const app = express();
app.use(express.json());

// Membuat posting baru
app.post('/posts', (req, res) => {
  const { title, content } = req.body;
  const post = postService.createPost(title, content);
  res.json(post);
});

// Mendapatkan semua posting
app.get('/posts', (req, res) => {
  const posts = postService.getPosts();
  res.json(posts);
});

app.listen(3000, () => {
  console.log('Post Service running on port 3000');
});
