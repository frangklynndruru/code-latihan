class EventBus {
   constructor() {
     this.listeners = {};
   }
 
   on(event, callback) {
     if (!this.listeners[event]) {
       this.listeners[event] = [];
     }
     this.listeners[event].push(callback);
   }
 
   emit(event, data) {
     const eventListeners = this.listeners[event];
     if (eventListeners) {
       eventListeners.forEach((listener) => {
         listener(data);
       });
     }
   }
 }
 
 module.exports = new EventBus();
 