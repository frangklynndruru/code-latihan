// eventBusService

const express = require('express');
const eventBus = require('./eventBus');

const app = express();
app.use(express.json());
let postsWithComments = [];

// Endpoint untuk menerima event dari API post
app.post('/events/post', (req, res) => {
   const { event, data } = req.body;
   eventBus.emit(event, data);
   const post = data;
   console.log(`event received :${event} :${JSON.stringify(data)}`)
   const postWithComments = { ...post, comments: [] };
   postsWithComments.push(postWithComments);
   res.sendStatus(200);
});

// Endpoint untuk menerima event dari API comment
app.post('/events/comment', (req, res) => {
  const { event, data } = req.body;
  eventBus.emit(event, data);
  console.log(`event received :${event} :${JSON.stringify(data)}`)
  console.log(postsWithComments)
  const postWithComments = postsWithComments.find((post) => post.id === parseInt(data.postId));
  console.log(postWithComments)

  if (postWithComments) {
    postWithComments.comments.push(data);
  }

  res.sendStatus(200);
});

app.get('/posts', (req, res) => {
   // Ambil data posting dengan komentar dari event bus
   res.json(postsWithComments);
 });

app.listen(5000, () => {
  console.log('Event Bus Service running on port 5000');
});
