// commentService.js

const axios = require('axios');

class CommentService {
  constructor() {
    this.comments = [];
    this.nextCommentId = 1;
  }

  createComment(postId, content) {
    const comment = { id: this.nextCommentId, postId, content };
    this.comments.push(comment);
    this.nextCommentId++;
    this.sendEvent('commentCreated', comment);
    console.log('event created: commentCreated')
    return comment;
  }

  sendEvent(event, data) {
    axios.post('http://localhost:5000/events/comment', { event, data });
  }

  getComments() {
    return this.comments;
  }
}

module.exports = new CommentService();
