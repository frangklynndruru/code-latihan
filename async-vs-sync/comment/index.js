// commentApp.js (Comment Service)

const express = require('express');
const commentService = require('./commentService');

const app = express();
app.use(express.json());

// Membuat komentar baru
app.post('/comments', (req, res) => {
  const { postId, content } = req.body;
  const comment = commentService.createComment(postId, content);
  res.json(comment);
});

// Mendapatkan semua komentar
app.get('/comments', (req, res) => {
  const comments = commentService.getComments();
  res.json(comments);
});

app.listen(4000, () => {
  console.log('Comment Service running on port 4000');
});
